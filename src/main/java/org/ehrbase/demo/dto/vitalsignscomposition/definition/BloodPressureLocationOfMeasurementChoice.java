package org.ehrbase.demo.dto.vitalsignscomposition.definition;

import javax.annotation.processing.Generated;

@Generated(
    value = "org.ehrbase.client.classgenerator.ClassGenerator",
    date = "2023-05-10T09:56:55.561854796Z",
    comments = "https://github.com/ehrbase/openEHR_SDK Version: 1.26.0"
)
public interface BloodPressureLocationOfMeasurementChoice {
}
