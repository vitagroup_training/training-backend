package org.ehrbase.demo.dto.bloodpressuredemohipv0composition.definition;

import javax.annotation.processing.Generated;

@Generated(
    value = "org.ehrbase.client.classgenerator.ClassGenerator",
    date = "2021-08-25T08:32:22.239936600+02:00",
    comments = "https://github.com/ehrbase/openEHR_SDK Version: 1.5.0")
public interface BloodPressureLocationOfMeasurementChoice {}
